@extends('layouts.public')
@section('page-name')Jadwal Diklat @endsection
@section('banner')
    @include('schedule.banner')
@endsection

@section('content')
    
    @include('schedule.form')
    @include('schedule.table')

    

@endsection