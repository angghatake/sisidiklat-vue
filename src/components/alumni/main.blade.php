@extends('layouts.public')

@section('page-name')Alumni @endsection
@section('banner')
    @include('alumnus.banner')
@endsection

@section('content')
    
    @include('alumnus.form')
    @include('alumnus.table')

    

@endsection