import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Jadwal from '../views/Jadwal.vue'
import Alumni from '../views/Alumni.vue'
import Faq from '../views/Faq.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
// import Maintenance from '../views/Maintenance.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/jadwal',
    name: 'Jadwal',
    component: Jadwal
  },
  {
    path: '/alumni',
    name: 'Alumni',
    component: Alumni
  },
  {
    path: '/faq',
    name: 'Faq',
    component: Faq
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
    // component: Maintenance
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
    // component: Maintenance
  },
  {
    path: '/diklat',
    beforeEnter() {location.href = 'https://bdidenpasar.kemenperin.go.id/bdi-beranda/layanan/diklat-3-in-1/'}
  },
  {
    path: '/bdi',
    beforeEnter() {location.href = 'https://bdidenpasar.kemenperin.go.id'}
  }
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
