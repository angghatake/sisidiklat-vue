# sisdiklat-vue
Untuk menjalakan aplikasi pastikan menggunakan nodejs

## Project structure

- Template : ./src/components
- Style : ./src/assets/sass

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
